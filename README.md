# Hotelovernachtingen in grote steden in Nederland

Niki van Koningsbruggen

26 september 2018

# Inleiding


# Inhoudsopgave

* [Inleiding](#inleiding)
* [Onderzoeksvraag](#onderzoeksvraag)
* [Hypothese](#hypothese)
    * [Cameragebieden](#cameragebieden)
    * [Misdaadcijfers](#misdaadcijfers)
    * [Oppervlaktes](#oppervlaktes)
    * [Datasets](#datasets)
* [Proces](#proces)
    * [Mogelijke onderwerpen](#Mogelijke onderwerpen)   
    * [Hypotheses](#hypotheses)   
    * [Zoekwoorden](#zoekwoorden)
    * [Buurtonderzoek](#buurtonderzoek)
* [Bronnen](#bronnen)
    * [Datasets](#datasets)
    * [Overige bronnen](#overig)

# Onderzoeksvraag

**Onderzoeksvraag:**
Hoe ontwikkelt het aantal hotelovernachtingen van binnenlandse en buitenlandse 
toeristen in de grote steden van Nederland zich in de periode 2012 t/m 2016 (blz. 226)

Doelgroep: binnenlandse en buitenlandse toeristen
Product: Aantal hotelovernachtingen
Context: Groei in de grote steden van Nederland over de periode van 2012 t/m 2016

I do research on (subject), because I want to find out (what, why, how), in order to (goal), so (interest).

Ik doe onderzoek naar het aantal hotelovernachtingen van binnenlandse en buitenlandse
toeristen in de grote steden van Nederland in de periode 2012 t/m 2016, om de groei 
te bepalen ten opzichte van voorgaande jaren, om hierdoor meer inzicht te krijgen 
in het toenemende toeristen aantal per grote stad in Nederland.

Onderwerp: Hoe erg groeit het binnenlandse en buitenlandse toerisme en hoe is de 
verdeling daarvan in de grote steden van Nederland.

Onderzoeken
Aantal hotelovernachtingen:
-	van binnenlandse en buitenlandse toeristen
-	In de grote steden van Nederland
-	In de periode van 2012 t/m 2016

# Hypothese

# Bronnen

### Datasets

**Datasets Statline** 

*Hotels; gasten, overnachtingen, woonland, regio*: https://opendata.cbs.nl/statline/#/CBS/nl/dataset/82061ned/table?ts=1537895882661

*Logiesaccommodaties; gasten, nachten, woonland, logiesvorm, regio*: https://opendata.cbs.nl/#/CBS/nl/dataset/82059NED/table?ts=1537908620645

*Ontwikkeling van het aantal vakanties naar vakantiekenmerken*: https://opendata.cbs.nl/#/CBS/nl/dataset/37526/table?ts=1537904726095

Cijfers per maand: *Logiesaccommodaties; gasten, overnachtingen, bezettingsgraad, kerncijfers*: https://opendata.cbs.nl/statline/#/CBS/nl/dataset/82058NED/table?ts=1537981804674

**Dataset World Tourism Organization**

https://knoema.com/WTODB2017/world-tourism-organization-database-2017?location=1001390-netherlands

### Overig

**(1) Trendrapport**

Samenstelling: NRIT Media, Centraal Bureau voor de Statistiek, NBTC Holland Marketing 
en CELTH, Centre of Expertise Leisure, Tourism & Hospitality (2017). *Trendrapport toerisme, 
recreatie en vrije tijd 2017.* URL: https://www.cbs.nl/-/media/_pdf/2017/47/trendrapport_deel_2.pdf

Zoektermen: 
-   Toerisme AND provincie

**(2) Toerisme groeit in meeste provincies** 

ING Economisch Bureau (2016, maart). *Toerisme groeit in meeste provincies*. 
URL:https://www.ing.nl/media/ING%20-%20Toerisme%20groeit%20in%20meeste%20provincies%20-%20maart%202016_tcm162-101300.pdf

Zoektermen:
-   Hotelovernachtingen AND provincies

**(3) Groei toerisme in meeste provincies**

REDACTIE GEMEENTE.NU (2016, 24 maart). *Groei toerisme in meeste provincies.* 
URL: https://www.gemeente.nu/bestuur/gemeenten/groei-toerisme-in-meeste-provincies/ 

Zoektermen:
-   Groei toerisme overnachtingen

**(4) Grootste groei toerisme in ruim tien jaar**

CBS (2018, 4 april). *Grootste groei toerisme in ruim tien jaar.* 
URL: https://www.cbs.nl/nl-nl/nieuws/2018/14/grootste-groei-toerisme-in-ruim-tien-jaar

**(5) Steeds meer hotelovernachtingen**

CBS (2017, 8 september). *Steeds meer hotelovernachtingen.* 
URL: https://www.cbs.nl/nl-nl/nieuws/2017/36/steeds-meer-hotelovernachtingen

Zoektermen en methode:
-   Sneeuwbal effect
-   Inkomend toerisme nederland

**(6) Inkomend toerisme**

NBTC (2017) *kerncijfers 2017.* 
URL: https://kerncijfers.nbtc.nl/nl/magazine/11936/821915/inkomend_verblijfsbezoek.html

**(7) Toerisme in Perspectief**

NBTC Holland Marketing (2018, juli). *Toerisme in Perspectief.* 
URL: https://nbtc.nl/web/file?uuid=d96b3697-c3fa-4be3-83a8-5a7fec8bfd92&owner=388ad020-d235-4624-86a4-d899f855a216&contentid=64358

**(8) Onderzoek inkomend toerisme**

Bunnik (2015, 23 april). *Onderzoek inkomend toerisme.* 
URL: https://www.nbtc.nl/web/file?uuid=ed844b64-2e5e-4416-b649-2b1314625beb&owner=388ad020-d235-4624-86a4-d899f855a216&contentid=47159

**(9) UNWTO Tourism Highlights**

UNWTO (2018). *UNWTO Tourism Highlights.* 
URL: https://www.e-unwto.org/doi/pdf/10.18111/9789284419876

**(10) UNWTO**

UNWTO World Tourism Organization (2017, 24 juli) *Netherlands: Country-specific: Overnight stays of non-resident tourists in hotels and similar establishments, by country of residence 2012 - 2016.* 
URL: https://www.e-unwto.org/doi/pdf/10.5555/unwtotfb0528101220122016201707

Zoekterm: 
-   Groei hotelovernachtingen
-   groei hotelovernachtingen site:data.overheid.nl/

**(11) Aantal hotelgasten in Utrecht in de jaren 2012 tm 2016**

https://data.overheid.nl/data/dataset/dpf-hotelbezoek-utrecht

Gemeente Utrecht (2016, 3 augustus) *Toerisme hotelgasten.* 
URL: https://ckan.dataplatform.nl/dataset/e1021ca7-229d-4a6b-bb8b-7c88db9d188b/resource/e33cef31-8f0b-4367-8239-0dc27c65577c/download/toerisme-hotelgasten.xlsx



